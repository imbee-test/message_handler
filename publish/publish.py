import boto3
from dotenv import load_dotenv
import mysql.connector
import os
import json

# Create SQS client
load_dotenv()
sqs = boto3.client('sqs', region_name='us-east-1')

queue_url = os.getenv('SQS_QUEUE_URL')
host=os.getenv('HOST')
database=os.getenv('DB')
username=os.getenv('USER')
password=os.getenv('PASSWORD')
# Receive message from SQS queue
def receive_message():
    response = sqs.receive_message(
        QueueUrl=queue_url,
        AttributeNames=[
            'SentTimestamp'
        ],
        MaxNumberOfMessages=1,
        MessageAttributeNames=[
            'All'
        ],
        VisibilityTimeout=0,
        WaitTimeSeconds=0
    )

    message = response['Messages'][0]
    receipt_handle = message['ReceiptHandle']
    print('Received message: %s' % message)
    return message

def update_db(message):
    # Open database connection
    db = mysql.connector.connect(
        host=host,
        user="queue_user",
        password=password,
        database=database
    )
    # prepare a cursor object using cursor() method
    cursor = db.cursor()

    # execute SQL query using execute() method.
    cursor.execute("SELECT VERSION()")

    # Fetch a single row using fetchone() method.
    data = cursor.fetchone()
    if data:
        print('Version available: ', data)
    else:
        print('Version not retrieved.')

    cursor.execute("CREATE TABLE IF NOT EXISTS status (uuid VARCHAR(255), status VARCHAR(255))")
    sql = "INSERT INTO status (uuid, status) VALUES (%s, %s)"
    val = (message['MessageAttributes']['message_id']['StringValue'], 'done')
    cursor.execute(sql, val)
    db.commit()
    print(cursor.rowcount, "record inserted.")
    # disconnect from server
    db.close()
    

def publish(message):
    response = sqs.send_message(
        QueueUrl=queue_url,
        MessageAttributes={
            'message_id': {
                'DataType': 'String',
                'StringValue': message['MessageAttributes']['message_id']['StringValue']
            },
            'user': {
                'DataType': 'String',
                'StringValue': message['MessageAttributes']['user']['StringValue']
            },
            'user_message': {
                'DataType': 'String',
                'StringValue': message['MessageAttributes']['user_message']['StringValue']
            }
        },
        MessageBody=(
            'This is the done message'
        ),
        MessageGroupId='done'
    )

    print(response['MessageId'])

def delete_message():
    # Get the service resource
    sqs = boto3.resource('sqs', region_name='us-east-1')

    # Get the queue
    queue = sqs.get_queue_by_name(QueueName='notification_fcm.fifo')

    # Process messages by printing out body
    for message in queue.receive_messages():
        # Let the queue know that the message is processed
        message.delete()
    print('deleted message: %s' % message)

message=receive_message()
update_db(message)
publish(message)
delete_message()