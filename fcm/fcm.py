import boto3
from dotenv import load_dotenv
import os
import uuid

# Create SQS client
load_dotenv()
sqs = boto3.client('sqs', region_name='us-east-1')

queue_url = os.getenv('SQS_QUEUE_URL')

# Send message to SQS queue
def send(message_id, user, user_message):
    response = sqs.send_message(
        QueueUrl=queue_url,
        MessageAttributes={
            'message_id': {
                'DataType': 'String',
                'StringValue': message_id
            },
            'user': {
                'DataType': 'String',
                'StringValue': user
            },
            'user_message': {
                'DataType': 'String',
                'StringValue': user_message
            }
        },
        MessageBody=(
            'Information about which messages that user sent to you'
        ),
        MessageGroupId="fcm"
    )

    print("UUID: " + message_id + " response: "+ response['MessageId'])

send(str(uuid.uuid4()), "John", "hi, how much of this product?")
#send(str(uuid.uuid4()), "3", "test 3")
